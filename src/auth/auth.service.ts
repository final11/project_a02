import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/entities/user.entity';
import { comparePassword } from 'src/utils/bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOneByUserName(username);
    // if (user && user.password === pass) {
    //   const { password, ...result } = user;
    //   return result;
    // }
    // return null;

    if (user) {
      const matched = comparePassword(pass, user.password);
      if (matched) {
        return user;
      } else {
        return null;
      }
    }
  }

  async login(user: User) {
    const payload = { username: user.username, sub: user.id };
    console.log(payload);
    return {
      user,
      access_token: this.jwtService.sign(payload),
    };
  }
}
