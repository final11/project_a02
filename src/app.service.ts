import { Injectable } from '@nestjs/common';
import * as os from 'os';
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  // getLocalIp(): string {
  //   const ifaces = os.networkInterfaces();
  //   let localIp = '';

  //   Object.keys(ifaces).forEach((ifname) => {
  //     if (ifname === 'Ethernet') {
  //       ifaces[ifname].forEach((iface) => {
  //         if (iface.family !== 'IPv4' || iface.internal !== false) {
  //           return; // skip over internal (i.e. 127.0.0.1) and non-IPv4 addresses
  //         }
  //         localIp = iface.address;
  //         console.log(iface);
  //       });
  //     }
  //     console.log(ifname);
  //   });

  //   return localIp;
  // }
}
