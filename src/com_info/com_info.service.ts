import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Room } from 'src/rooms/entities/room.entity';
import { Repository } from 'typeorm';
import { CreateComInfoDto } from './dto/create-com_info.dto';
import { UpdateComInfoDto } from './dto/update-com_info.dto';
import { ComInfo } from './entities/com_info.entity';

@Injectable()
export class ComInfoService {
  constructor(
    @InjectRepository(ComInfo)
    private cominfoRepository: Repository<ComInfo>,
  ) {}

  create(createComInfoDto: CreateComInfoDto[]): Promise<ComInfo[]> {
    return this.cominfoRepository.save(createComInfoDto);
  }

  findAll(): Promise<ComInfo[]> {
    return this.cominfoRepository.find();
  }

  findOne(id: number): Promise<ComInfo> {
    return this.cominfoRepository.findOneBy({ id });
  }

  findByRoomName(roomName: string): Promise<ComInfo[]> {
    return this.cominfoRepository.findBy({ roomName });
  }

  findOneByIp(ip: string): Promise<ComInfo> {
    return this.cominfoRepository.findOneBy({ ip });
  }

  findByMac(mac: string): Promise<ComInfo[]> {
    return this.cominfoRepository.findBy({ mac });
  }

  findOneByPositoinRoomName(
    position: number,
    roomName: string,
  ): Promise<ComInfo[]> {
    return this.cominfoRepository.findBy({ position, roomName });
  }

  update(id: number, updateMacDto: UpdateComInfoDto) {
    return this.cominfoRepository.update(id, updateMacDto);
  }

  async remove(id: string): Promise<void> {
    await this.cominfoRepository.delete(id);
  }
}
