import { Mac } from 'src/mac/entities/mac.entity';
import { Room } from 'src/rooms/entities/room.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class ComInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ip: string;

  @Column()
  position: number;

  @Column()
  roomName: string;

  @Column()
  mac: string;

  // @ManyToOne(() => Room, (room) => room.comInfos, {
  //   eager: true,
  //   cascade: true,
  //   onDelete: 'CASCADE',
  //   onUpdate: 'CASCADE',
  // })
  // @JoinColumn({ name: 'roomId' })
  // room: Room;

  // @OneToMany(() => Mac, (mac) => mac.comInfo)
  // macs: Mac[];
}
