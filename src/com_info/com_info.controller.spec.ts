import { Test, TestingModule } from '@nestjs/testing';
import { ComInfoController } from './com_info.controller';
import { ComInfoService } from './com_info.service';

describe('ComInfoController', () => {
  let controller: ComInfoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ComInfoController],
      providers: [ComInfoService],
    }).compile();

    controller = module.get<ComInfoController>(ComInfoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
