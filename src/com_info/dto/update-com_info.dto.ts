import { PartialType } from '@nestjs/mapped-types';
import { CreateComInfoDto } from './create-com_info.dto';

export class UpdateComInfoDto extends PartialType(CreateComInfoDto) {}
