export class CreateComInfoDto {
  id: number;
  ip: string;
  position: number;
  roomName: string;
  mac: string;
}
