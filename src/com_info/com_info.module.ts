import { Module } from '@nestjs/common';
import { ComInfoService } from './com_info.service';
import { ComInfoController } from './com_info.controller';
import { ComInfo } from './entities/com_info.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ComInfo])],
  controllers: [ComInfoController],
  providers: [ComInfoService],
  exports: [ComInfoService],
})
export class ComInfoModule {}
