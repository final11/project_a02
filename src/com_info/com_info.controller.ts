import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { Room } from 'src/rooms/entities/room.entity';
import { ComInfoService } from './com_info.service';
import { CreateComInfoDto } from './dto/create-com_info.dto';
import { UpdateComInfoDto } from './dto/update-com_info.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('com-info')
export class ComInfoController {
  constructor(private readonly comInfoService: ComInfoService) {}

  @Post()
  create(@Body() createComInfoDto: CreateComInfoDto[]) {
    console.log(createComInfoDto);
    return this.comInfoService.create(createComInfoDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.comInfoService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.comInfoService.findOne(+id);
  }

  @Get('room/:roomName')
  findByRoomName(@Param('roomName') roomName: string) {
    return this.comInfoService.findByRoomName(roomName);
  }

  @Get('mac/:mac')
  findByMac(@Param('mac') mac: string) {
    return this.comInfoService.findByRoomName(mac);
  }

  @Get('search/:ip')
  findOneByIp(@Param('ip') ip: string) {
    return this.comInfoService.findOneByIp(ip);
  }

  @Post('searchPR/:roomName')
  findOneByPositoinRoomName(
    @Body('position') position: number,
    @Param('roomName') roomName: string,
  ) {
    return this.comInfoService.findOneByPositoinRoomName(position, roomName);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateComInfoDto: UpdateComInfoDto) {
    return this.comInfoService.update(+id, updateComInfoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.comInfoService.remove(id);
  }
}
