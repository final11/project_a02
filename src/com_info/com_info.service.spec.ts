import { Test, TestingModule } from '@nestjs/testing';
import { ComInfoService } from './com_info.service';

describe('ComInfoService', () => {
  let service: ComInfoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ComInfoService],
    }).compile();

    service = module.get<ComInfoService>(ComInfoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
