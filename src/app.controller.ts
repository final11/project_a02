import {
  Controller,
  Get,
  Post,
  UploadedFile,
  UseInterceptors,
  Param,
  UseGuards,
  Req,
  Ip,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AppService } from './app.service';
import { diskStorage } from 'multer';
import readXlsxFile from 'read-excel-file/node';
import { CreateComInfoDto } from './com_info/dto/create-com_info.dto';
import { ComInfoService } from './com_info/com_info.service';
// import { getConnection } from 'typeorm';
import * as xlsx from 'xlsx';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { AuthService } from './auth/auth.service';
import * as fs from 'fs';
import { Request } from 'express';
import { RealIP } from 'nestjs-real-ip';
import { ComInfo } from './com_info/entities/com_info.entity';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly comInfoService: ComInfoService,
    private authService: AuthService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req: any, file: any, cb: any) => {
          cb(null, `${file.originalname}`);
        },
      }),
    }),
  )
  async end() {
    return;
  }

  @Post('uploadTable')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req: any, file: any, cb: any) => {
          cb(null, `${file.originalname}`);
        },
      }),
    }),
  )
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    const response = {
      originalname: file.originalname,
      filename: file.filename,
    };
    const workbook = xlsx.readFile(`./uploads/${file.filename}`);
    const sheet_name_list = workbook.SheetNames;
    const xlData: CreateComInfoDto[] = xlsx.utils.sheet_to_json(
      workbook.Sheets[sheet_name_list[0]],
    );
    console.log(xlData);
    await this.comInfoService.create(xlData);
    fs.unlinkSync(`./uploads/${file.filename}`);
    return response;
  }

  @Get('upload/:roomUiName')
  readExcel(@Param('roomUiName') roomUiName: string) {
    // File path.
    return readXlsxFile('./uploads/' + roomUiName);
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Req() req) {
    return this.authService.login(req.user);
  }

  // @Get('localip')
  // getLocalIp(): string {
  //   return this.appService.getLocalIp();
  // }

  @Get('reqIp')
  getIpAddress(@Req() request: Request): string {
    const ipAddress = request.ip;
    console.log(request);
    return ipAddress;
  }

  @Get('/ip')
  getIpAddress2(@Ip() ip) {
    console.log(ip);
    return ip;
  }

  // @Get('realIp')
  // getIpAddress3(@RealIP() ip: string): string {
  //   return ip;
  // }

  // @Get('realIp')
  // getIpAddress4(@RealIP() ip: string): Promise<ComInfo> {
  //   ip = '192.168.1.40';
  //   let realIp = '';
  //   for (let i = 0; i < ip.length; i++) {
  //     if (i >= 7) {
  //       realIp += ip.charAt[i];
  //       console.log(realIp);
  //     }
  //   }
  //   console.log(realIp);
  //   return this.comInfoService.findOneByIp(realIp);
  // }

  @Get('realIp')
  getIpAddress3(@RealIP() ip: string): Promise<ComInfo> {
    // ip = ':fffff:192.168.1.40';
    // ip = ip.substring(7);
    for (let i = 0; i < ip.length; i++) {
      ip = ip.replace('f', '');
      ip = ip.replace(':', '');
      console.log(ip);
    }
    console.log(ip);
    return this.comInfoService.findOneByIp(ip);
  }
}
