import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTimeInfoDto } from './dto/create-time_info.dto';
import { UpdateTimeInfoDto } from './dto/update-time_info.dto';
import { TimeInfo } from './entities/time_info.entity';

@Injectable()
export class TimeInfoService {
  constructor(
    @InjectRepository(TimeInfo)
    private timeInfoRepository: Repository<TimeInfo>,
  ) {}

  create(createTimeInfoDto: CreateTimeInfoDto): Promise<TimeInfo> {
    return this.timeInfoRepository.save(createTimeInfoDto);
  }

  findAll(): Promise<TimeInfo[]> {
    return this.timeInfoRepository.find();
  }

  findOne(id: number): Promise<TimeInfo> {
    return this.timeInfoRepository.findOneBy({ id });
  }

  update(id: number, updateMacDto: UpdateTimeInfoDto) {
    return this.timeInfoRepository.update(id, updateMacDto);
  }

  updateC(
    year: string,
    month: string,
    day: number,
    hour: number,
    minute: number,
    second: number,
    updateTimeInfoDto: UpdateTimeInfoDto,
  ) {
    return this.timeInfoRepository.update(
      {
        year,
        month,
        day,
        hour,
        minute,
        second,
      },
      updateTimeInfoDto,
    );
  }

  async remove(id: string): Promise<void> {
    await this.timeInfoRepository.delete(id);
  }

  findByPositoinRoomName(
    position: number,
    roomName: string,
  ): Promise<TimeInfo[]> {
    return this.timeInfoRepository.findBy({ position, roomName });
  }
}
