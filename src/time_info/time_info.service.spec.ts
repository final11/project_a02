import { Test, TestingModule } from '@nestjs/testing';
import { TimeInfoService } from './time_info.service';

describe('TimeInfoService', () => {
  let service: TimeInfoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TimeInfoService],
    }).compile();

    service = module.get<TimeInfoService>(TimeInfoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
