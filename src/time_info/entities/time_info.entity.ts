import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TimeInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ip: string;

  @Column({ nullable: true })
  roomName: string;

  @Column({ nullable: true })
  position: number;

  @Column()
  year: string;

  @Column()
  month: string;

  @Column()
  day: number;

  @Column()
  hour: number;

  @Column()
  shutdownHour: number;

  @Column()
  minute: number;

  @Column()
  shutdownMinute: number;

  @Column()
  second: number;
}
