import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Put,
  UseGuards,
} from '@nestjs/common';
import { TimeInfoService } from './time_info.service';
import { CreateTimeInfoDto } from './dto/create-time_info.dto';
import { UpdateTimeInfoDto } from './dto/update-time_info.dto';
import { ComInfoService } from '../com_info/com_info.service';
import { TimeInfo } from './entities/time_info.entity';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('time-info')
export class TimeInfoController {
  constructor(
    private readonly timeInfoService: TimeInfoService,
    private readonly comInfoService: ComInfoService,
  ) {}

  @Post()
  create(@Body() createTimeInfoDto: CreateTimeInfoDto) {
    this.comInfoService.findOneByIp(createTimeInfoDto.ip).then((result) => {
      if (result) {
        createTimeInfoDto.position = result.position;
        createTimeInfoDto.roomName = result.roomName;
      } else {
        createTimeInfoDto.position = 0;
        createTimeInfoDto.roomName = '';
      }
      //console.log(createTimeInfoDto);
      createTimeInfoDto.hour = +createTimeInfoDto.hour;
      createTimeInfoDto.minute = +createTimeInfoDto.minute;
      createTimeInfoDto.shutdownHour = +createTimeInfoDto.shutdownHour;
      createTimeInfoDto.shutdownMinute = +createTimeInfoDto.shutdownMinute;
      //console.log(createTimeInfoDto);
      this.timeInfoService.create(createTimeInfoDto);
    });

    return createTimeInfoDto;
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.timeInfoService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.timeInfoService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTimeInfoDto: UpdateTimeInfoDto,
  ) {
    return this.timeInfoService.update(+id, updateTimeInfoDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('searchPR/:roomName')
  findOneByPositoinRoomName(
    @Body('position') position: number,
    @Param('roomName') roomName: string,
  ) {
    return this.timeInfoService.findByPositoinRoomName(position, roomName);
  }

  @Put('update/:year/:month/:day/:hour/:minute/:second')
  updateC(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('day') day: string,
    @Param('hour') hour: string,
    @Param('minute') minute: string,
    @Param('second') second: string,
    @Body() updateTimeInfoDto: UpdateTimeInfoDto,
  ) {
    // this.timeInfoService.findAll().then((result) => {
    //   if (result) {
    //     updateTimeInfoDto.id = result[result.length - 1].id + 1;
    //   } else {
    //     updateTimeInfoDto.id = 1;
    //   }
    // });
    return this.timeInfoService.updateC(
      year,
      month,
      +day,
      +hour,
      +minute,
      +second,
      updateTimeInfoDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.timeInfoService.remove(id);
  }
}
