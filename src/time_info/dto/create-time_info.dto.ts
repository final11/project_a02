export class CreateTimeInfoDto {
  id: number;

  ip: string;

  roomName: string;

  position: number;

  year: string;

  month: string;

  day: number;

  hour: number;

  shutdownHour: number;

  minute: number;

  shutdownMinute: number;

  second: number;
}
