import { PartialType } from '@nestjs/mapped-types';
import { CreateTimeInfoDto } from './create-time_info.dto';

export class UpdateTimeInfoDto extends PartialType(CreateTimeInfoDto) {}
