import { Module } from '@nestjs/common';
import { TimeInfoService } from './time_info.service';
import { TimeInfoController } from './time_info.controller';
import { TimeInfo } from './entities/time_info.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComInfoModule } from 'src/com_info/com_info.module';

@Module({
  imports: [TypeOrmModule.forFeature([TimeInfo]), ComInfoModule],
  controllers: [TimeInfoController],
  providers: [TimeInfoService],
})
export class TimeInfoModule {}
