import { Test, TestingModule } from '@nestjs/testing';
import { TimeInfoController } from './time_info.controller';
import { TimeInfoService } from './time_info.service';

describe('TimeInfoController', () => {
  let controller: TimeInfoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TimeInfoController],
      providers: [TimeInfoService],
    }).compile();

    controller = module.get<TimeInfoController>(TimeInfoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
