import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MacModule } from './mac/mac.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mac } from './mac/entities/mac.entity';
import { DataSource } from 'typeorm';
import { RoomsModule } from './rooms/rooms.module';
import { Room } from './rooms/entities/room.entity';
import { ComInfoModule } from './com_info/com_info.module';
import { ComInfo } from './com_info/entities/com_info.entity';
import { MulterModule } from '@nestjs/platform-express';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { TimeInfoModule } from './time_info/time_info.module';
import { TimeInfo } from './time_info/entities/time_info.entity';
import { InformModule } from './inform/inform.module';
import { Inform } from './inform/entities/inform.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'com_info',
      password: 'pz)9)HSLr52Tee',
      database: 'com_info',
      entities: [Mac, Room, ComInfo, User, TimeInfo, Inform],
      synchronize: true,
    }),
    MacModule,
    RoomsModule,
    ComInfoModule,
    MulterModule.register({
      dest: './uploads',
    }),
    UsersModule,
    AuthModule,
    TimeInfoModule,
    InformModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
