import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { InformService } from './inform.service';
import { CreateInformDto } from './dto/create-inform.dto';
import { UpdateInformDto } from './dto/update-inform.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('inform')
export class InformController {
  constructor(private readonly informService: InformService) {}

  @Post()
  create(@Body() createInformDto: CreateInformDto) {
    return this.informService.create(createInformDto);
  }

  @Get()
  findAll() {
    return this.informService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.informService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateInformDto: UpdateInformDto) {
    return this.informService.update(+id, updateInformDto);
  }

  @Get('confirm/:confirm')
  findByConfirm(@Param('confirm') confirm: string) {
    return this.informService.findByConfirm(confirm);
  }

  @Post('searchByCF/:confirm')
  findByFinishAndConfirm(
    @Param('confirm') confirm: string,
    @Body('finish') finish: string,
  ) {
    return this.informService.findByFinishAndConfirm(confirm, finish);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.informService.remove(id);
  }

  @Post('searchPR/:roomName')
  findOneByPositoinRoomName(
    @Body('position') position: number,
    @Param('roomName') roomName: string,
    @Body('finish') finish: string,
  ) {
    return this.informService.findOneByPositoinRoomName(
      position,
      roomName,
      finish,
    );
  }
}
