import { Module } from '@nestjs/common';
import { InformService } from './inform.service';
import { InformController } from './inform.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Inform } from './entities/inform.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Inform])],
  controllers: [InformController],
  providers: [InformService],
})
export class InformModule {}
