import { Test, TestingModule } from '@nestjs/testing';
import { InformController } from './inform.controller';
import { InformService } from './inform.service';

describe('InformController', () => {
  let controller: InformController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InformController],
      providers: [InformService],
    }).compile();

    controller = module.get<InformController>(InformController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
