import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityRepository, Repository } from 'typeorm';
import { CreateInformDto } from './dto/create-inform.dto';
import { UpdateInformDto } from './dto/update-inform.dto';
import { Inform } from './entities/inform.entity';

@Injectable()
export class InformService {
  constructor(
    @InjectRepository(Inform)
    private informRepository: Repository<Inform>,
  ) {}

  create(createInformDto: CreateInformDto): Promise<Inform> {
    return this.informRepository.save(createInformDto);
  }

  findAll(): Promise<Inform[]> {
    return this.informRepository.find();
  }

  findOne(id: number): Promise<Inform> {
    return this.informRepository.findOneBy({ id });
  }

  findByConfirm(confirm: string): Promise<Inform[]> {
    // const sql = 'SELECT * FROM inform WHERE confirm = ? ORDER BY id DESC';
    return this.informRepository.find({
      where: {
        confirm,
      },
      order: {
        id: 'DESC',
      },
    });
  }

  findByFinishAndConfirm(confirm: string, finish: string): Promise<Inform[]> {
    return this.informRepository.find({
      where: {
        confirm,
        finish,
      },
      order: {
        id: 'DESC',
      },
    });
  }

  update(id: number, updateInformDto: UpdateInformDto) {
    return this.informRepository.update(id, updateInformDto);
  }

  async remove(id: string): Promise<void> {
    await this.informRepository.delete(id);
  }

  findOneByPositoinRoomName(
    position: number,
    roomName: string,
    finish: string,
  ): Promise<Inform[]> {
    return this.informRepository.find({
      where: {
        position,
        roomName,
        finish,
      },
      order: {
        id: 'DESC',
      },
    });
  }
}
