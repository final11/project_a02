export class CreateInformDto {
  id: number;

  form: string;

  confirm: string;

  finish: string;

  roomName: string;

  position: number;

  newTime: string;

  finishTime: string;

  message: string;

  confirmTime: string;

  mac: string;
}
