import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Inform {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  form: string;

  @Column()
  confirm: string;

  @Column()
  finish: string;

  @Column()
  roomName: string;

  @Column()
  position: number;

  @Column({ nullable: true })
  newTime: string;

  @Column({ nullable: true })
  finishTime: string;

  @Column({ nullable: true })
  confirmTime: string;

  @Column()
  message: string;

  @Column()
  mac: string;
}
