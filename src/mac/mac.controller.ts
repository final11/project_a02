import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Put,
} from '@nestjs/common';
import { MacService } from './mac.service';
import { CreateMacDto } from './dto/create-mac.dto';
import { UpdateMacDto } from './dto/update-mac.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('mac')
export class MacController {
  constructor(private readonly macService: MacService) {}

  @Post()
  create(@Body() createMacDto: CreateMacDto) {
    // const timeOpen = createMacDto.timeOpen;
    // const timeOpenArray = timeOpen.split('');

    // for (let i = 0; i < timeOpenArray.length; i++) {
    //   if (timeOpenArray[i] === '/') {
    //     timeOpenArray[i] = '-';
    //   }
    // }
    // createMacDto.timeOpen = timeOpenArray.join('');
    return this.macService.create(createMacDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.macService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.macService.findOne(+id);
  }

  // @Get('cominfo/:comInfoId')
  // findByComInfoId(@Param('comInfoId') comInfoId: number) {
  //   return this.macService.findByComInfoId(+comInfoId);
  // }

  @UseGuards(JwtAuthGuard)
  @Get('search/:ip')
  findOneByIP(@Param('ip') ip: string) {
    return this.macService.findOneByIP(ip);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMacDto: UpdateMacDto) {
    return this.macService.update(+id, updateMacDto);
  }

  @Put('update/:timeOpen')
  updateC(
    @Param('timeOpen') timeOpen: string,
    @Body() updateMacDto: UpdateMacDto,
  ) {
    return this.macService.updateC(timeOpen, updateMacDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.macService.remove(id);
  }
}
