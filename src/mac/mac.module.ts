import { Module } from '@nestjs/common';
import { MacService } from './mac.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mac } from './entities/mac.entity';
import { MacController } from './mac.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Mac])],
  controllers: [MacController],
  providers: [MacService],
})
export class MacModule {}
