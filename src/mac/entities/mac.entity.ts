import { ComInfo } from 'src/com_info/entities/com_info.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Mac {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ip: string;

  @Column()
  mac: string;

  @Column({ nullable: true })
  timeOpen: string;

  @Column({ nullable: true })
  lastTimeShutdown: string;

  @Column()
  cpu: string;

  @Column()
  mainboard: string;

  @Column()
  ram: string;

  // @Column()
  // comInfoId: number;

  // @ManyToOne(() => ComInfo, (comInfo) => comInfo.macs, {
  //   eager: true,
  //   cascade: true,
  //   onDelete: 'CASCADE',
  //   onUpdate: 'CASCADE',
  // })
  // @JoinColumn({ name: 'comInfoId' })
  // comInfo: ComInfo;
}
