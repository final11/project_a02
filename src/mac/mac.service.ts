import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMacDto } from './dto/create-mac.dto';
import { UpdateMacDto } from './dto/update-mac.dto';
import { Mac } from './entities/mac.entity';

@Injectable()
export class MacService {
  constructor(
    @InjectRepository(Mac)
    private macRepository: Repository<Mac>,
  ) {}

  create(createMacDto: CreateMacDto): Promise<Mac> {
    return this.macRepository.save(createMacDto);
  }

  findAll(): Promise<Mac[]> {
    return this.macRepository.find();
  }

  findOne(id: number): Promise<Mac> {
    return this.macRepository.findOneBy({ id });
  }

  // findByComInfoId(comInfoId: number): Promise<Mac[]> {
  //   return this.macRepository.findBy({ comInfoId });
  // }

  findOneByIP(ip: string): Promise<Mac[]> {
    return this.macRepository.findBy({ ip });
  }

  update(id: number, updateMacDto: UpdateMacDto) {
    return this.macRepository.update(id, updateMacDto);
  }

  updateC(timeOpen: string, updateMacDto: UpdateMacDto) {
    return this.macRepository.update({ timeOpen }, updateMacDto);
  }

  async remove(id: string): Promise<void> {
    await this.macRepository.delete(id);
  }
}
