import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateRoomDto } from './dto/create-room.dto';
import { UpdateRoomDto } from './dto/update-room.dto';
import { Room } from './entities/room.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RoomsService {
  constructor(
    @InjectRepository(Room)
    private roomRepository: Repository<Room>,
  ) {}

  create(createRoomDto: CreateRoomDto): Promise<Room> {
    return this.roomRepository.save(createRoomDto);
  }

  findAll(): Promise<Room[]> {
    return this.roomRepository.find();
  }

  findOne(id: number): Promise<Room> {
    return this.roomRepository.findOneBy({ id });
  }

  // findOne(id: number): Promise<Room> {
  //   return this.roomRepository.findOne({
  //     where: { id },
  //     relations: ['comInfos'],
  //   });
  //   return this.roomRepository.query('SELECT * FROM ROOM');
  // }

  findOneByname(roomName: string): Promise<Room> {
    return this.roomRepository.findOneBy({ roomName });
  }

  update(id: number, updateRoomDto: UpdateRoomDto) {
    return this.roomRepository.update(id, updateRoomDto);
  }

  async remove(id: string): Promise<void> {
    await this.roomRepository.delete(id);
  }
}
