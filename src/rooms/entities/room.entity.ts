import { ComInfo } from 'src/com_info/entities/com_info.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  roomName: string;

  // @OneToMany(() => ComInfo, (comInfo) => comInfo.room)
  // comInfos: ComInfo[];
}
